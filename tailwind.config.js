/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
      extend: {
      brightness: {
        65: '.65'
      }
    },
    fontFamily: {
      'standard': ["Inria Sans"],
    },
  },
  plugins: [],
}

