import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GeocodingService {
  private apiUrl = 'https://nominatim.openstreetmap.org/search';

  constructor(private http: HttpClient) { }

  geocode(address: string): Observable<any> {
    const encodedAddress = encodeURIComponent(address);
    
    return this.http.get<any>(`${this.apiUrl}?q=${encodedAddress}&format=json`);
  }
}
