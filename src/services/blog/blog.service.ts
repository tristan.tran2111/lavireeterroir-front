import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { Blog } from '../../models/blog.model';
import { environment } from '../../environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient) {}

  getAllBlogs(): Observable<Blog[]>{
    return this.http.get<Blog[]>(`${environment.ENDPOINT_URL}/blog/getBlogs`);
  }

  getBlogByName(name: string): Observable<Blog>{
    return this.http.get<Blog>(`${environment.ENDPOINT_URL}/blog/${name}`);
  }

  getPaginatedBlogs(page: number, itemsPerPage: number): Observable<any> {
    const params = { page: page.toString(), itemsPerPage: itemsPerPage.toString() };
    return this.http.get<any>(`${environment.ENDPOINT_URL}/blog/getPaginatedBlogs`, { params });
  }

  getRecentBlogs(): Observable<Blog[]> {
    return this.http.get<Blog[]>(`${environment.ENDPOINT_URL}/blog/getBlogs`).pipe(
      map((blogs: any[]) => blogs.sort((a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()).slice(0, 3))
    );
  }

  createBlog(blogData: FormData): Observable<any> {
    return this.http.post<any>(`${environment.ENDPOINT_URL}/blog/createBlog`, blogData);
  }

  getBlogsByDepartmentCode(code: number): Observable<Blog[]> {
    return this.http.get<any>(`${environment.ENDPOINT_URL}/blog/department/${code}`);
  }
}
