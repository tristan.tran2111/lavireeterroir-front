import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.development';
import { Product, ProductType } from '../../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  getAllProducts(): Observable<Product[]> {
    return this.http.get<any>(`${environment.ENDPOINT_URL}/product/getProducts`);
  }

  getProductByName(product_name: string): Observable<Product> {
    return this.http.get<any>(`${environment.ENDPOINT_URL}/product/${product_name}/getProductByName`);
  }

  getAllProductsType(): Observable<ProductType[]> {
    return this.http.get<any>(`${environment.ENDPOINT_URL}/product-type/getProductsType`);
  }

  getProductByDepartmentCode(department_code: number): Observable<Product[]> {
    return this.http.get<any>(`${environment.ENDPOINT_URL}/product/getProductsByDepartment/${department_code}`);
  }
}
