import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.development';
import { IBooking } from '../../models/booking.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WorkshopService {

  constructor(private http: HttpClient) { }

  getWorkshopById(id: number) {
    return this.http.get<any>(`${environment.ENDPOINT_URL}/workshop/${id}`);
  }

  getWorkshopByDepartmentCode(department_code: number) {
    return this.http.get<any>(`${environment.ENDPOINT_URL}/workshop/department/${department_code}`);
  }

  getReservationDataById(id: number) {
    return this.http.get<any>(`${environment.ENDPOINT_URL}/reservation-dates/${id}`);
  }

  bookingWorkshop(token: string, data: IBooking) {
    return this.http.post<any>(`${environment.ENDPOINT_URL}/bookings/${token}`, data);
  }

  deleteBooking(token: string, bookingId: number): Observable<any> {
    return this.http.delete<any>(`${environment.ENDPOINT_URL}/bookings/${bookingId}/${token}`);
  }
}
