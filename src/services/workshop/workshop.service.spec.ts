import { TestBed } from '@angular/core/testing';

import { WorkshopService } from './workshop.service';
import { HttpClientModule } from '@angular/common/http';

describe('WorkshopService', () => {
  let service: WorkshopService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(WorkshopService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
