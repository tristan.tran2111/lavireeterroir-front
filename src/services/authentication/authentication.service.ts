import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ILogin, IRegister } from '../../models/authentication.model';
import { environment } from '../../environments/environment.development';
import { CookieOptions, CookieService } from 'ngx-cookie-service';
import { Observable, tap } from 'rxjs';
import { Profile, User } from '../../models/user.model';
import { IMerchant } from '../../models/merchant.model';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {

  constructor(private http: HttpClient, private cookieService: CookieService) { }

  // For get user information (C'est un objet et non pas un array car il n'y a pas plusieurs informations user mais seulement un par un)
  getUserProfile(username: string): Observable<User> {
    return this.http.get<User>(`${environment.ENDPOINT_URL}/user/${username}/getUserByUsername`);
  }

  getUserBookings(token: string): Observable<any> {
    return this.http.get<any>(`${environment.ENDPOINT_URL}/user/bookings/${token}`)
  }

  // For login
  signIn(credentials: ILogin): Observable<any> {
    return this.http.post<any>(`${environment.ENDPOINT_URL}/user/signIn`, credentials)
      .pipe(
        tap(res => {
          const cookieOptions: CookieOptions = {
            secure: true,
            sameSite: 'Strict',
            path: '/'
          };
          this.cookieService.set('username', res.data, cookieOptions);
          this.cookieService.set('authentication', res.token, cookieOptions);
          this.cookieService.set('merchant', res.merchantToken, cookieOptions);
        })
      );
  }

  // For checking validity of token
  validateToken(token: string): Observable<{ valid: boolean }> {
    return this.http.get<{ valid: boolean }>(`${environment.ENDPOINT_URL}/user/validateToken?token=${token}`);
  }

  // For checking if user is a merchant
  isMerchant(username: string): Observable<{ valid: boolean }> {
    return this.http.get<{ valid: boolean }>(`${environment.ENDPOINT_URL}/user/isMerchant/${username}`);
  }

  // For registering (sending email of confirmation)
  signUp(data: IRegister) {
    return this.http.post<any>(`${environment.ENDPOINT_URL}/user/signUp`, data);
  }

  // For logout
  signOut(): void {
    this.cookieService.delete('username', '/');
    this.cookieService.delete('authentication', '/');
    this.cookieService.delete('merchant', '/');
  }

  // For registering (Create user with token of confirmation)
  confirmUser(token: string): Observable<any> {
    return this.http.get<any>(`${environment.ENDPOINT_URL}/user/confirm/${token}`)
  }

  // For reset password
  resetPassword(token: string, password: any): Observable<any> {
    return this.http.put<any>(`${environment.ENDPOINT_URL}/user/updatePassword/${token}`, password)
  }

  // For sending forgot password mail (Pas mis de model parce il n'y a qu'un champ)
  sendForgotEmail(mail: any): Observable<any> {
    return this.http.post<any>(`${environment.ENDPOINT_URL}/user/forgot`, mail)
  }

  // For sending creation merchant mail
  sendMailcreateMerchant(data: IMerchant): Observable<any> {
    return this.http.post<any>(`${environment.ENDPOINT_URL}/merchants/sendMailCreateCommerce`, data);
  }

  // For registering and associating merchant with user
  confirmCreateMerchant(username: string, token: string): Observable<any> {
    return this.http.get<any>(`${environment.ENDPOINT_URL}/merchants/confirm/${username}/${token}`);
  }

  // For getting user wishlist
  getWishlist(token: string): Observable<any> {
    return this.http.get<any>(`${environment.ENDPOINT_URL}/user/wishlist?token=${token}`);
  }

  // For adding a product to the user's wishlist
  addToWishlist(token: string, productId: number): Observable<any> {
    return this.http.post<any>(`${environment.ENDPOINT_URL}/user/wishlist/${productId}?token=${token}`, {});
  }

   // For deleting a product from the user's wishlist
   deleteWishlist(token: string, productId: number): Observable<any> {
    return this.http.delete<any>(`${environment.ENDPOINT_URL}/user/wishlist/${productId}?token=${token}`);
  }

  // For adding a circuit to the user's followed circuits
  addCircuitToUser(token: string, circuitId: number): Observable<any> {
    return this.http.post<any>(`${environment.ENDPOINT_URL}/user/circuits/${circuitId}?token=${token}`, {});
  }

  // For getting user's followed circuits
  getUserCircuits(token: string): Observable<any> {
    return this.http.get<any>(`${environment.ENDPOINT_URL}/user/circuits?token=${token}`);
  }

  // For removing a circuit from the user's followed circuits
  removeCircuitFromUser(token: string, circuitId: number): Observable<any> {
    return this.http.delete<any>(`${environment.ENDPOINT_URL}/user/circuits/${circuitId}?token=${token}`);
  }

  // For checking if a user follows a specific circuit
  isCircuitFollowedByUser(token: string, circuitId: number): Observable<{ followed: boolean }> {
    return this.http.get<{ followed: boolean }>(`${environment.ENDPOINT_URL}/user/circuits/${circuitId}/is-followed?token=${token}`);
  }

  updateUserProfile(token: string, data: Profile): Observable<any> {
    return this.http.put<any>(`${environment.ENDPOINT_URL}/user/updateProfile/${token}`, data, {
    });
  }
}
