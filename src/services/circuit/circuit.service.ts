import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class CircuitService {

  constructor(private http: HttpClient) { }

  getCircuitByDepartmentCode(code: number) {
    return this.http.get<any>(`${environment.ENDPOINT_URL}/circuit/department/${code}`);
  }

  getCircuitById(id: number) {
    return this.http.get<any>(`${environment.ENDPOINT_URL}/circuit/${id}`)
  }
}
