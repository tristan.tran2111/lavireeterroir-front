import { TestBed } from '@angular/core/testing';

import { CircuitService } from './circuit.service';
import { HttpClientModule } from '@angular/common/http';

describe('CircuitService', () => {
  let service: CircuitService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(CircuitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
