import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  constructor(private http: HttpClient) {}

  addAMerchantComment(token: string, merchantId: number, commentData: any) {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.http.post(`${environment.ENDPOINT_URL}/review/${merchantId}`, commentData, { headers });
  }

  addAWorshopComment(token: string, workshopId: number, commentData: any) {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.http.post(`${environment.ENDPOINT_URL}/review/${workshopId}/workshop`, commentData, { headers });
  }
}
