import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.development';
import { Observable } from 'rxjs';
import { Department } from '../../models/department.model';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  constructor(private http: HttpClient) {}

  getAllDepartments(): Observable<Department[]>{
    return this.http.get<Department[]>(`${environment.ENDPOINT_URL}/department/getDepartments`);
  }

  getDepartment(department_code: number): Observable<Department>{
    return this.http.get<Department>(`${environment.ENDPOINT_URL}/department/${department_code}/getDepartmentByCode`);
  }

  createDepartment(departmentData: FormData): Observable<Department> {
    return this.http.post<Department>(`${environment.ENDPOINT_URL}/department/createDepartment`, departmentData);
  }
}
