import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class MerchantService {

  constructor(private http: HttpClient) {}

  getDataOfMerchant(token: string) : Observable<any> {
    return this.http.get<any>(`${environment.ENDPOINT_URL}/merchants/getMerchantData/${token}`)
  }

  getMerchantByName(name: string) : Observable<any> {
    return this.http.get<any>(`${environment.ENDPOINT_URL}/merchants/${name}`)
  }
}
