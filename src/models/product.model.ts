export interface Product {
  ID_product: number;
  product_name: string;
  product_description: string;
  product_img: string;
  ProductType: ProductType;
  ProductDietCategory: ProductDietCategory;
  merchants: any;
}

export interface ProductType {
  ID_product_type: number;
  product_type_name: string;
}

export interface ProductDietCategory {
  ID_product_diet_category: number;
  product_diet_category_name: string;
}