import { Department } from "./department.model";

export interface Blog {
  ID_article: number;
  article_title: string;
  article_content: string;
  article_image: string;
  creation_date: Date;
  ID_department: Department
}