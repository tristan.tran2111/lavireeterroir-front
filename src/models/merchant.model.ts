export interface IMerchant {
  merchant_name: string;
  merchant_address: string;
  merchant_email: string;
  merchant_phone: string;
}