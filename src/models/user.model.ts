export interface User {
  ID_user: number;
  user_name: string;
  user_mail: string;
  avatar: string;
  password: string;
  biography: string;
  firstname: string;
  user_phone: string;
  user_address: string;
  lastname: string;
  year: number;
  admin: boolean
}

export interface Profile {
  ID_user: number;
  user_name: string;
  user_mail: string;
  password: string;
  biography: string;
  firstname: string;
  user_phone: string;
  user_address: string;
  lastname: string;
  year: number;
}