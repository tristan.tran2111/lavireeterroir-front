export interface ILogin {
  user_mail: string;
  password: string;
}

export interface IRegister {
  user_name: string;
  user_mail: string;
  password: string;
}