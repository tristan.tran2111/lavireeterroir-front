export interface IBooking {
  workshopId: number,
  reservationDateId: number,
  booking_date: Date,
  number_of_persons: number,
  total_price: number
}