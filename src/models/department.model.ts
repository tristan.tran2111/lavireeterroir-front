export interface Department {
  ID_department: number;
  department_name: string;
  department_description: string;
  department_img: string;
  department_code: number;
}