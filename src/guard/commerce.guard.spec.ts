import { TestBed } from '@angular/core/testing';
import { CanActivateFn } from '@angular/router';

import { commerceGuard } from './commerce.guard';

describe('commerceGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => commerceGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});
