import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AuthenticationService } from '../services/authentication/authentication.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

export const authGuard: CanActivateFn = (): Observable<boolean> => {
  const router = inject(Router);
  const cookieService = inject(CookieService);
  const authService = inject(AuthenticationService);
  const token = cookieService.get('authentication');
  const user = cookieService.get('username');

  if (token && user) {
    return authService.validateToken(token).pipe(
      map(response => response.valid),
      map(valid => {
        if (valid) {
          return true;
        } else {
          router.navigate(['connect-to-your-account']);
          return false;
        }
      })
    );
  } else {
    router.navigate(['connect-to-your-account']);
    return of(false);
  }
};
