import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AuthenticationService } from '../services/authentication/authentication.service';
import { Observable, of } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';

export const adminGuard: CanActivateFn = (): Observable<boolean> => {
  const router = inject(Router);
  const cookieService = inject(CookieService);
  const authService = inject(AuthenticationService);
  const token = cookieService.get('authentication');
  const username = cookieService.get('username');

  if (token && username) {
    return authService.validateToken(token).pipe(
      switchMap(response => {
        if (response.valid) {
          return authService.getUserProfile(username).pipe(
            map(user => {
              if (user.admin) {
                return true;
              } else {
                router.navigate(['']);
                return false;
              }
            })
          );
        } else {
          router.navigate(['connect-to-your-account']);
          return of(false);
        }
      })
    );
  } else {
    router.navigate(['connect-to-your-account']);
    return of(false);
  }
};
