import { CommonModule } from '@angular/common';
import { Component, HostListener, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterLink } from '@angular/router';
import { ButtonComponent } from '../../component/button/button.component';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [RouterLink, CommonModule, ButtonComponent],
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss'
})
export class HeaderComponent implements OnInit {
  showNav: boolean = false;
  scrolled: boolean = false;
  currentRoute!: string;
  showSearchBar: boolean = false;

  constructor(private router: Router) { }

  ngOnInit() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.currentRoute = event.url;
        console.log(this.currentRoute)
      }
    });
  }

  toggleSearchBar(): void {
    this.showSearchBar = !this.showSearchBar;
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    this.scrolled = window.scrollY > 0;
  }

  closeNav() {
    this.showNav = false;
  }

  openNav() {
    this.showNav = true;
  }

  navigateToCommerce() {
    this.showNav = false;
    this.router.navigate(['my-commerce']);
  }

  notShowHeader(): boolean {
    const currentRoute = this.currentRoute || '';
    return currentRoute === '/' || currentRoute.indexOf('/circuit/') === 0;
  }


}
