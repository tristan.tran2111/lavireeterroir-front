import { Component, OnInit, Renderer2 } from '@angular/core';
import { ButtonComponent } from '../../component/button/button.component';
import { CarouselComponent } from '../../component/carousel/carousel.component';
import { BlogService } from '../../../services/blog/blog.service';
import { Blog } from '../../../models/blog.model';
import { CommonModule } from '@angular/common';
import { MapComponent } from '../../component/map/map.component';
import { Router } from '@angular/router';
import { ProductService } from '../../../services/products/product.service';
import { Product } from '../../../models/product.model';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [ButtonComponent, CarouselComponent, MapComponent, CommonModule],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent implements OnInit {
  blog: Blog[] = [];
  productList: Product[] = [];
  currentBackgroundOpacity!: number;
  currentBackground: string = "../../../assets/terroirentete01.jpg";
  backgrounds: string[] = [
    "../../../assets/terroirentete01.jpg",
    "../../../assets/agri.jpg",
    "../../../assets/leaf-1811533_1280.jpg"
  ];

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    navSpeed: 700,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 2
      },
      1000: {
        items: 4
      },
      1400: {
        items: 5
      },
      1600: {
        items: 6
      }
    },
    nav: false,
    autoplay: true,
    autoplayTimeout: 3000,
    autoplayHoverPause: true
  };

  constructor(
    private blogService: BlogService,
    private productService: ProductService,
    private renderer: Renderer2,
    private router: Router
  ) { }

  ngOnInit() {
    this.fetchRecentBlog();
    this.getProductData();
    this.preloadBackgroundsAndStartChanging();
  }

  fetchRecentBlog() {
    this.blogService.getRecentBlogs().subscribe((articles) => {
      this.blog = articles;
    });
  }

  getProductData() {
    this.productService.getAllProducts().subscribe((items) => {
      this.productList = items.slice(0, 10);
    });
  }

  preloadBackgroundsAndStartChanging() {
    let loadedImagesCount = 0;
    const totalImagesCount = this.backgrounds.length;

    const defaultImage = this.renderer.createElement('img');
    defaultImage.onload = () => {
      loadedImagesCount++;
      if (loadedImagesCount === totalImagesCount) {
        this.changeBackgroundPeriodically();
      }
    };
    defaultImage.src = this.currentBackground;

    for (let i = 0; i < this.backgrounds.length; i++) {
      const img = this.renderer.createElement('img');
      img.onload = () => {
        loadedImagesCount++;
        if (loadedImagesCount === totalImagesCount) {
          this.changeBackgroundPeriodically();
        }
      };
      img.src = this.backgrounds[i];
    }
  }

  changeBackgroundPeriodically() {
    let index = 0;
    setInterval(() => {
      this.currentBackgroundOpacity = 0;
      setTimeout(() => {
        this.currentBackground = this.backgrounds[index];
        this.currentBackgroundOpacity = 1;
      }, 500);
      index = (index + 1) % this.backgrounds.length;
    }, 4500);
  }

  navigateToAllDepartmentPage() {
    this.router.navigate(['/all-department']);
    window.scrollTo(0, 0);
  }

  navigateToProduct(productName: string) {
    this.router.navigate(['/product', productName]);
    window.scrollTo(0, 0);
  }

  navigateToBlog(articleTitle: string) {
    this.router.navigate(['/blog', articleTitle]);
    window.scrollTo(0, 0);
  }

  navigateToCircuit(id: number) {
    this.router.navigate(['/circuit', id]);
    window.scroll(0, 0);
  }
}
