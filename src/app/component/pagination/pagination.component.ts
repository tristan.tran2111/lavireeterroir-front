import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'app-pagination',
  standalone: true,
  imports: [],
  templateUrl: './pagination.component.html',
  styleUrl: './pagination.component.scss'
})
export class PaginationComponent implements OnChanges {
  @Input() totalItems!: number;
  @Input() itemsPerPage!: number;
  @Output() pageChange = new EventEmitter<number>();

  currentPage: number = 1;
  totalPagesArray: number[] = [];

  ngOnChanges() {
    this.generatePageNumbersArray();
  }

  generatePageNumbersArray() {
    this.totalPagesArray = [];
    const totalPages = Math.ceil(this.totalItems / this.itemsPerPage);
    for (let i = 1; i <= totalPages; i++) {
      this.totalPagesArray.push(i);
    }
  }

  onPageChange(page: number) {
    this.currentPage = page;
    this.pageChange.emit(page);
  }
}
