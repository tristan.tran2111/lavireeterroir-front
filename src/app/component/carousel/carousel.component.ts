import { Component, Input, TemplateRef } from '@angular/core';
import { CarouselModule, OwlOptions } from 'ngx-owl-carousel-o';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-carousel',
  standalone: true,
  imports: [CommonModule, CarouselModule],
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent {

  @Input() dataList!: any[];
  @Input() itemTemplate!: TemplateRef<any>;
  @Input() customOptions!: OwlOptions;
}
