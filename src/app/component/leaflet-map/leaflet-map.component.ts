import { Component, AfterViewInit, Input } from '@angular/core';

@Component({
  selector: 'app-leaflet-map',
  standalone: true,
  imports: [],
  templateUrl: './leaflet-map.component.html',
  styleUrls: ['./leaflet-map.component.scss']
})
export class LeafletMapComponent implements AfterViewInit {
  private map: any;
  @Input() latitude!: number;
  @Input() longitude!: number;
  @Input() showMarker: boolean = true;

  constructor() { }

  async ngAfterViewInit() {
    if (typeof window !== 'undefined') {
      const L = await import('leaflet');
      this.initMap(L);
    }
  }

  private initMap(L: any): void {
    console.log('Leaflet loaded', L);

    this.map = L.map('map', {
      center: [this.latitude, this.longitude],
      zoom: 13
    });

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);

    if (this.showMarker) {
      const customIcon = L.icon({
        iconUrl: 'assets/circuit.png', // Chemin relatif corrigé
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34]
      });

      L.marker([this.latitude, this.longitude], { icon: customIcon }).addTo(this.map)
        .bindPopup('Par ici !')
        .openPopup();
    }
  }
}
