import { Component, HostListener } from '@angular/core';
import { Department } from '../../../models/department.model';
import { Router } from '@angular/router';
import { DepartmentService } from '../../../services/department/department.service';

@Component({
  selector: 'app-map',
  standalone: true,
  imports: [],
  templateUrl: './map.component.html',
  styleUrl: './map.component.scss'
})
export class MapComponent {
  isHovering: boolean = false;
  department_name!: string;
  department_link!: Department;
  tooltipX: number = 0;
  tooltipY: number = 0;

  constructor(
    private router: Router,
    private departmentService: DepartmentService
  ){}

  @HostListener('mousemove', ['$event'])
  onMouseMove(event: MouseEvent) {
    if (this.isHovering) {
      this.tooltipX = event.clientX + 10;
      this.tooltipY = event.clientY - 20;
    }
  }

  onPathHover(code: number) {
    this.isHovering = true;
    this.departmentService.getDepartment(code).subscribe(data => {
      this.department_name = data.department_name;
    });
  }

  onPathLeave() {
    this.isHovering = false;
  }

  navigateToDepartment(departmentCode: number) {
    this.router.navigate(['/department', departmentCode]);
  }
}
