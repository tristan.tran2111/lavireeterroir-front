import { Component, Input, Output, EventEmitter, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselComponent } from '../carousel/carousel.component';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-gallery',
  standalone: true,
  imports: [CommonModule, CarouselComponent],
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements AfterViewInit {
  @Input() isOpen: boolean = false;
  @Input() images: string[] = [];
  @Output() closeGallery = new EventEmitter<void>();  // Add this line to emit the close event

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    navSpeed: 700,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      },
      1400: {
        items: 1
      },
      1600: {
        items: 1
      }
    },
    nav: true,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    autoplay: false,
    autoplayTimeout: 3000,
    autoplayHoverPause: true
  };

  constructor(private cdr: ChangeDetectorRef) {}

  ngAfterViewInit() {
    if (this.isOpen) {
      setTimeout(() => {
        this.cdr.detectChanges();
      }, 0);
    }
  }

  close() {
    this.isOpen = false;
    this.closeGallery.emit();
  }
}
