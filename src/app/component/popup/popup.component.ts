import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-popup',
  standalone: true,
  imports: [],
  templateUrl: './popup.component.html',
  styleUrl: './popup.component.scss'
})
export class PopupComponent implements OnInit {
  @Input() message: string = '';
  @Input() duration: number = 3000;

  ngOnInit() {
    if (this.message) {
      setTimeout(() => {
        this.message = '';
      }, this.duration);
    }
  }
}
