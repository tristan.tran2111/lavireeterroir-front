import { CommonModule } from '@angular/common';
import { ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './modal.component.html',
  styleUrl: './modal.component.scss'
})
export class ModalComponent {
  @Input() isOpen: boolean = false;
  @Input() title: string = '';
  @Output() closed = new EventEmitter<void>();

  constructor(private cdr: ChangeDetectorRef){}

  close() {
    this.isOpen = false;
    this.closed.emit();
    this.cdr.detectChanges();
  }
}
