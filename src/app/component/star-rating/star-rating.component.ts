import { CommonModule } from '@angular/common';
import { Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-star-rating',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss']
})
export class StarRatingComponent implements OnInit, OnChanges {
  @Input() rating: number = 0;
  @Input() editable: boolean = false;
  @Output() ratingChange: EventEmitter<number> = new EventEmitter<number>();
  stars: Array<string> = [];

  ngOnInit(): void {
    this.stars = this.calculateStars(this.rating);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['rating']) {
      this.stars = this.calculateStars(changes['rating'].currentValue);
    }
  }

  calculateStars(rating: number): Array<string> {
    const stars = [];
    for (let i = 1; i <= 5; i++) {
      if (rating >= i) {
        stars.push('fa-star');
      } else if (rating >= i - 0.5) {
        stars.push('fa-star-half-alt');
      } else {
        stars.push('fa-star-o');
      }
    }
    return stars;
  }

  onStarClick(index: number): void {
    if (this.editable) {
      this.rating = index + 1;
      this.stars = this.calculateStars(this.rating);
      this.ratingChange.emit(this.rating);
    }
  }

  trackByIndex(index: number): any {
    return index;
  }
}
