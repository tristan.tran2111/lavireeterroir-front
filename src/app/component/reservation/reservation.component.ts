import { CommonModule, DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, Output, OnChanges, SimpleChanges } from '@angular/core';
import { ButtonComponent } from '../button/button.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reservation',
  standalone: true,
  imports: [DatePipe, ButtonComponent, CommonModule],
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnChanges {
  @Input() reservationList: any[] = [];
  @Input() isOpen: boolean = false;
  @Input() reservation_name!: any;
  @Output() closeReservation = new EventEmitter<void>(); 

  groupedReservations: { [date: string]: any[] } = {};
  reservationDates: string[] = [];

  constructor(private router: Router){}

  ngOnChanges(changes: SimpleChanges) {
    if (changes['reservationList']) {
      this.groupReservationsByDate();
    }
  }

  close() {
    this.isOpen = false;
    this.closeReservation.emit();
  }

  formatTime(time: string): Date {
    const [hours, minutes] = time.split(':').map(Number);
    const date = new Date();
    date.setHours(hours, minutes, 0);
    return date;
  }

  groupReservationsByDate() {
    this.groupedReservations = this.reservationList.reduce((groups, reservation) => {
      const reservationDate = new Date(reservation.reservation_date);
      const date = reservationDate.toISOString().split('T')[0];
      if (!groups[date]) {
        groups[date] = [];
      }
      groups[date].push(reservation);
      return groups;
    }, {});
    this.reservationDates = Object.keys(this.groupedReservations);
  }

  navigateToBooking(id: number) {
    this.router.navigate(['/booking', id]);
    window.scrollTo(0, 0);
  }
}
