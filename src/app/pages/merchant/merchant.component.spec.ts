import { TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

describe('MerchantComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get: () => '1'
              }
            }
          }
        }
      ]
    })
    .compileComponents();
  });

  it('should pass', () => {
    expect(true).toBe(true);
  });
});
