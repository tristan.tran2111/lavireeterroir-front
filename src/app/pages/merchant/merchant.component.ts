import { Component, OnInit } from '@angular/core';
import { MerchantService } from '../../../services/merchant/merchant.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { GalleryComponent } from '../../component/gallery/gallery.component';
import { GeocodingService } from '../../../services/leaflet/geocoding.service';
import { LeafletMapComponent } from '../../component/leaflet-map/leaflet-map.component';
import { StarRatingComponent } from '../../component/star-rating/star-rating.component';
import { ModalComponent } from '../../component/modal/modal.component';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ReviewService } from '../../../services/review/review.service';
import { CookieService } from 'ngx-cookie-service';
import { ButtonComponent } from '../../component/button/button.component';
import { SpinnerComponent } from '../../component/spinner/spinner.component';
import { CarouselComponent } from '../../component/carousel/carousel.component';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { PopupComponent } from '../../component/popup/popup.component';

@Component({
  selector: 'app-merchant',
  standalone: true,
  imports: [
    CommonModule,
    GalleryComponent,
    LeafletMapComponent,
    StarRatingComponent,
    DatePipe,
    ModalComponent,
    ReactiveFormsModule,
    ButtonComponent,
    SpinnerComponent,
    CarouselComponent,
    PopupComponent
  ],
  templateUrl: './merchant.component.html',
  styleUrl: './merchant.component.scss'
})
export class MerchantComponent implements OnInit {
  merchantImageUrl!: string | null;
  secondMerchantImageUrl!: string | null;
  merchant: any;
  isGalleryOpen: boolean = false;
  isReviewOpen: boolean = false;
  merchantName!: string | null;
  photoUrls: string[] = [];
  latitude!: number;
  longitude!: number;
  averageRating!: number;
  token!: string;
  reviewForm!: FormGroup;
  displayedReviewsCount: number = 3;
  displayedReviewsCountDesktop: number = 4;
  isLoading: boolean = false;
  successMessage: string = '';

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    navSpeed: 700,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 2
      },
      1000: {
        items: 4
      },
      1400: {
        items: 5
      },
      1600: {
        items: 6
      }
    },
    nav: false,
    autoplay: true,
    autoplayTimeout: 3000,
    autoplayHoverPause: true
  };

  constructor(
    private merchantService: MerchantService,
    private route: ActivatedRoute,
    private router: Router,
    private geocodingService: GeocodingService,
    private reviewService: ReviewService,
    private cookieService: CookieService,
    private authService: AuthenticationService,
    private fb: FormBuilder
  ) {
    this.merchantName = this.route.snapshot.paramMap.get('merchant_name');
    this.token = this.cookieService.get('authentication');
    this.reviewForm = this.fb.group({
      rating: [null, [Validators.required]],
      content: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    if (this.merchantName !== null) {
      this.merchantService.getMerchantByName(this.merchantName).subscribe((data) => {
        this.merchant = data;
        this.merchantImageUrl = this.merchant.photos.length > 0 ? this.merchant.photos[0].url : null;
        this.secondMerchantImageUrl = this.merchant.photos.length > 0 ? this.merchant.photos[1].url : null;
        this.photoUrls = this.merchant.photos.map((photo: any) => photo.url);
        this.geocodeAddress(this.merchant.merchant_address);
        this.calculateAverageRating();
      });
    }
  }

  calculateAverageRating(): void {
    if (this.merchant && this.merchant.reviews && this.merchant.reviews.length > 0) {
      const totalRating = this.merchant.reviews.reduce((sum: any, review: { rating: any; }) => sum + review.rating, 0);
      this.averageRating = parseFloat((totalRating / this.merchant.reviews.length).toFixed(1));
    } else {
      this.averageRating = 0;
    }
  }

  geocodeAddress(address: string) {
    this.geocodingService.geocode(address).subscribe((data: any) => {
      if (data && data.length > 0) {
        this.latitude = parseFloat(data[0].lat);
        this.longitude = parseFloat(data[0].lon);
        console.log(`Latitude: ${this.latitude}, Longitude: ${this.longitude}`);
      } else {
        console.log('No coordinates found for the given address');
      }
    },
      (error) => {
        console.error('Error occurred while geocoding the address:', error);
      });
  }

  openGallery() {
    this.isGalleryOpen = true;
  }

  openReview() {
    this.isReviewOpen = true;
  }

  closeReview() {
    this.isReviewOpen = false;
  }

  closeGallery() {
    this.isGalleryOpen = false;
  }

  navigateToWorkshop(id: number) {
    this.router.navigate(['/workshop', id]);
    window.scrollTo(0, 0);
  }

  formatTime(time: string): Date {
    const [hours, minutes] = time.split(':').map(Number);
    const date = new Date();
    date.setHours(hours, minutes, 0);
    return date;
  }

  submitReview() {
    if (!this.token) {
      this.router.navigate(['/connect-to-your-account']);
      window.scrollTo(0, 0);
    } else {
      const formValue = this.reviewForm.value;
      this.isLoading = true;
      this.reviewService.addAMerchantComment(this.token, this.merchant.ID_merchant, formValue).subscribe({
        next: () => {
          this.isLoading = false;
          window.location.reload();
        },
        error: (err) => {
          this.isLoading = false;
          console.error(err);
        }
      });
    }
  }

  toggleWishlist(product: any): void {
    if (!this.token) {
      this.router.navigate(['/connect-to-your-account']);
      window.scrollTo(0, 0);
      return;
    }

    product.liked = !product.liked;

    if (product.liked) {
      this.authService.addToWishlist(this.token, product.ID_product).subscribe({
        next: () => {
          this.successMessage = 'Produit ajouté avec succès!';
          setTimeout(() => {
            this.successMessage = '';
          }, 5000);
        },
        error: (err) => {
          product.liked = false;
          console.error(`Error adding product to wishlist`, err);
        }
      });
    }
  }

  showMoreReviews() {
    this.displayedReviewsCount = this.merchant.reviews.length;
  }

}
