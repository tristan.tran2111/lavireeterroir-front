import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../../services/blog/blog.service';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { AngularEditorConfig, AngularEditorModule } from '@kolkov/angular-editor';
import { DepartmentService } from '../../../services/department/department.service';
import { Department } from '../../../models/department.model';
import { SpinnerComponent } from '../../component/spinner/spinner.component';
import { PopupComponent } from '../../component/popup/popup.component';

@Component({
  selector: 'app-admin',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    AngularEditorModule,
    SpinnerComponent,
    PopupComponent
  ],
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  blogForm: FormGroup;
  departmentForm: FormGroup;
  departments!: Department[];
  isLoading: boolean = false;
  successMessage: string = '';

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    sanitize: false,
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    toolbarPosition: 'top'
  };

  constructor(
    private fb: FormBuilder,
    private blogService: BlogService,
    private departmentService: DepartmentService
  ) {
    this.blogForm = this.fb.group({
      article_title: ['', Validators.required],
      article_content: ['', Validators.required],
      article_image: [null, Validators.required],
      ID_Department: ['']
    });

    this.departmentForm = this.fb.group({
      department_name: ['', Validators.required],
      department_description: ['', Validators.required],
      department_img: [null, Validators.required], // Updated to use file input
      department_code: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.loadDepartments();
  }

  loadDepartments() {
    this.departmentService.getAllDepartments().subscribe(departments => {
      this.departments = departments;
      console.log(this.departments);
    });
  }

  onFileChange(event: any, formControlName: string, formGroup: FormGroup) {
    const file = event.target.files[0];
    if (file) {
      formGroup.patchValue({
        [formControlName]: file
      });
    }
  }

  onSubmitBlog() {
    this.isLoading = true;
    if (this.blogForm.valid) {
      const formData = new FormData();
      formData.append('article_title', this.blogForm.get('article_title')?.value);
      formData.append('article_content', this.blogForm.get('article_content')?.value);
      formData.append('article_image', this.blogForm.get('article_image')?.value);
      formData.append('ID_Department', this.blogForm.get('ID_Department')?.value);

      this.blogService.createBlog(formData).subscribe({
        next: () => {
          this.isLoading = false;
          this.successMessage = 'Blog created successfully!';
          this.blogForm.reset();
          setTimeout(() => {
            this.successMessage = '';
          }, 5000);
        },
        error: (err) => {
          console.error('Error creating blog', err);
        }
      });
    }
  }

  onSubmitDepartment() {
    this.isLoading = true;
    if (this.departmentForm.valid) {
      const formData = new FormData();
      formData.append('department_name', this.departmentForm.get('department_name')?.value);
      formData.append('department_description', this.departmentForm.get('department_description')?.value);
      formData.append('department_img', this.departmentForm.get('department_img')?.value);
      formData.append('department_code', this.departmentForm.get('department_code')?.value);

      this.departmentService.createDepartment(formData).subscribe({
        next: () => {
          this.isLoading = false;
          this.successMessage = 'Department created successfully!';
          this.departmentForm.reset();
          this.loadDepartments(); // Refresh the list of departments
          setTimeout(() => {
            this.successMessage = '';
          }, 5000);
        },
        error: (err) => {
          console.error('Error creating department', err);
        }
      });
    }
  }
}
