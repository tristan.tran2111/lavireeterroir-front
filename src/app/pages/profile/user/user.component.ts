import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import { User } from '../../../../models/user.model';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { CommonModule } from '@angular/common';
import { ModalComponent } from '../../../component/modal/modal.component';
import { ButtonComponent } from '../../../component/button/button.component';
import { SpinnerComponent } from '../../../component/spinner/spinner.component';
import { WorkshopService } from '../../../../services/workshop/workshop.service';

@Component({
  selector: 'app-user',
  standalone: true,
  imports: [
    ModalComponent,
    CommonModule,
    ButtonComponent,
    ReactiveFormsModule,
    SpinnerComponent
  ],
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  modalIsOpen: boolean = false;
  modalEditUser: boolean = false;
  editUserForm!: FormGroup;
  loggedUsername!: string;
  userProfile!: User;
  allBooking: any;
  activeTab: string = 'profile';
  wishlist: any[] = [];
  circuits: any[] = [];
  isLoading: boolean = false;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private cookieService: CookieService,
    private workshopService: WorkshopService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.initEditUserForm();
    this.getUserData();
    this.getUserBooking();
    this.getUserWishlist();
    this.getUserCircuits();
  }

  initEditUserForm() {
    this.editUserForm = this.fb.group({
      user_name: ['', Validators.required],
      user_mail: ['', [Validators.required, Validators.email]],
      biography: [''],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      address: ['', Validators.required],
      year: ['', Validators.required],
    });
  }

  getUserData() {
    this.authenticationService.getUserProfile(this.cookieService.get('username')).subscribe(data => {
      this.userProfile = data;
      this.editUserForm.patchValue(this.userProfile);
    })
  }

  getUserBooking() {
    this.authenticationService.getUserBookings(this.cookieService.get('authentication')).subscribe(data => {
      this.allBooking = data;
    })
  }

  getUserWishlist() {
    this.authenticationService.getWishlist(this.cookieService.get('authentication')).subscribe(
      data => {
        this.wishlist = data;
        console.log(this.wishlist)
      },
      error => {
        console.error('Error fetching wishlist', error);
      }
    );
  }

  getUserCircuits() {
    this.authenticationService.getUserCircuits(this.cookieService.get('authentication')).subscribe(
      data => {
        this.circuits = data;
        console.log(this.circuits);
      },
      error => {
        console.error('Error fetching circuits', error);
      }
    );
  }

  navigateToWorkshop(id: number) {
    this.router.navigate(['/workshop', id]);
    window.scroll(0, 0);
  }

  navigateToCircuit(id: number) {
    this.router.navigate(['/circuit', id]);
    window.scroll(0, 0);
  }

  setActiveTab(tab: string) {
    this.activeTab = tab;
  }

  openModal() {
    this.modalIsOpen = true;
  }

  closeModal() {
    this.modalIsOpen = false;
  }

  openEditUserModal() {
    this.modalEditUser = true;
    this.editUserForm.patchValue(this.userProfile);
  }

  closeEditUserModal() {
    this.modalEditUser = false;
  }

  logout() {
    this.authenticationService.signOut();
    this.router.navigate([''])
  }

  onSubmit() {
    if (this.editUserForm.valid) {
      this.isLoading = true;
      this.authenticationService.updateUserProfile(this.cookieService.get('authentication'), this.editUserForm.value)
        .subscribe({
          next: () => {
            this.isLoading = false;
            this.closeEditUserModal();
            this.getUserData();
          },
          error: (error) => {
            console.error('Error updating profile', error);
          }
        });
    }
  }

  removeFromWishlist(productId: number) {
    this.authenticationService.deleteWishlist(this.cookieService.get('authentication'), productId)
      .subscribe(response => {
        console.log('Product removed from wishlist', response);
        this.getUserWishlist();
      }, error => {
        console.error('Error removing product from wishlist', error);
      });
  }

  deleteBooking(bookingId: number) {
    this.workshopService.deleteBooking(this.cookieService.get('authentication'), bookingId).subscribe({
      next: () => {
        this.getUserBooking();
      },
      error: (err) => {
        console.error('Error deleting booking', err);
      }
    });
  }
}
