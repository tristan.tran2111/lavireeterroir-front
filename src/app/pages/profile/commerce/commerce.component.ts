import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MerchantService } from '../../../../services/merchant/merchant.service';
import { CookieService } from 'ngx-cookie-service';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from '../../../component/button/button.component';
import { ModalComponent } from '../../../component/modal/modal.component';
import { Router } from '@angular/router';
import { GeocodingService } from '../../../../services/leaflet/geocoding.service';
import { LeafletMapComponent } from '../../../component/leaflet-map/leaflet-map.component';
import { StarRatingComponent } from '../../../component/star-rating/star-rating.component';

@Component({
  selector: 'app-commerce',
  standalone: true,
  imports: [
    CommonModule, 
    ButtonComponent, 
    ModalComponent,
    LeafletMapComponent,
    ReactiveFormsModule,
    StarRatingComponent
  ],
  templateUrl: './commerce.component.html',
  styleUrls: ['./commerce.component.scss']
})
export class CommerceComponent implements OnInit {
  firstImageUrl: string | null = null;
  secondImageUrl: string | null = null;
  MerchantData!: any;
  modalNameDesc: boolean = false;
  isGalleryOpen: boolean = false;
  icon: string = "<i class='fa-solid fa-pen'></i>";
  token!: string;
  latitude!: number;
  longitude!: number;
  photoUrls: string[] = [];
  imageForm!: FormGroup;
  displayedReviewsCount: number = 3;

  constructor(
    private merchantService: MerchantService,
    private cookieService: CookieService,
    private router: Router,
    private geocodingService: GeocodingService,
    private fb: FormBuilder
  ) {
    this.token = this.cookieService.get('merchant');
  }

  ngOnInit() {
    this.getData();
    this.imageForm = this.fb.group({
      newImageFile: [null]
    });
  }

  getData() {
    this.merchantService.getDataOfMerchant(this.token).subscribe((response: any) => {
      this.MerchantData = response.merchant;
      this.firstImageUrl = this.MerchantData.photos.length > 0 ? this.MerchantData.photos[0].url : null;
      this.secondImageUrl = this.MerchantData.photos.length > 0 ? this.MerchantData.photos[1].url : null;
      this.photoUrls = this.MerchantData.photos.map((photo: any) => photo.url);
      this.geocodeAddress(this.MerchantData.merchant_address);
    });
  }

  geocodeAddress(address: string) {
    this.geocodingService.geocode(address).subscribe((data: any) => {
      if (data && data.length > 0) {
        this.latitude = parseFloat(data[0].lat);
        this.longitude = parseFloat(data[0].lon);
        console.log(`Latitude: ${this.latitude}, Longitude: ${this.longitude}`);
      } else {
        console.log('No coordinates found for the given address');
      }
    },
    (error) => {
      console.error('Error occurred while geocoding the address:', error);
    });
  }

  openGallery() {
    this.isGalleryOpen = true;
  }

  closeGallery() {
    this.isGalleryOpen = false;
  }

  addPhoto(event: Event) {
    const input = event.target as HTMLInputElement;
    if (input.files && input.files[0]) {
      const file = input.files[0];
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const newImageUrl = e.target.result;
        this.MerchantData.photos.push({ url: newImageUrl });
        this.photoUrls.push(newImageUrl);
        this.imageForm.reset();
      };
      reader.readAsDataURL(file);
    }
  }

  removePhoto(index: number) {
    this.MerchantData.photos.splice(index, 1);
    this.photoUrls.splice(index, 1);
  }

  openModal() {
    this.modalNameDesc = true;
  }

  navigateToWorkshop(id: number) {
    this.router.navigate(['/workshop', id]);
    window.scrollTo(0, 0);
  }

  showMoreReviews() {
    this.displayedReviewsCount = this.MerchantData.reviews.length;
  }

  formatTime(time: string): Date {
    const [hours, minutes] = time.split(':').map(Number);
    const date = new Date();
    date.setHours(hours, minutes, 0);
    return date;
  }
}
