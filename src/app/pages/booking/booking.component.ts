import { CommonModule, DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ButtonComponent } from '../../component/button/button.component';
import { WorkshopService } from '../../../services/workshop/workshop.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { CookieService } from 'ngx-cookie-service';
import { ModalComponent } from '../../component/modal/modal.component';

@Component({
  selector: 'app-booking',
  standalone: true,
  imports: [ReactiveFormsModule, CommonModule, ButtonComponent, DatePipe, ModalComponent],
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {
  currentStep: number = 1;
  reservationImg!: string;
  stepsValidated: boolean[] = [false, false, false];
  bookingForm!: FormGroup;
  reservationId!: number;
  reservation!: any;
  totalAmount: number = 0;
  username!: string;
  isLoading: boolean = false;
  blockCbExpanded: boolean = false;
  blockPayPalExpanded: boolean = false;
  workshopName!: string;

  constructor(
    private fb: FormBuilder,
    private workshopService: WorkshopService,
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private router: Router,
    private cookieService: CookieService,
    private datePipe: DatePipe
  ) {
    this.reservationId = +this.route.snapshot.paramMap.get('id')!;
    this.username = this.cookieService.get('username');
  }

  ngOnInit(): void {
    this.bookingForm = this.fb.group({
      number_of_persons: [1, Validators.required],
      total_price: [null],
      booking_date: [this.datePipe.transform(new Date(), 'yyyy-MM-dd')],
      reservationDateId: [this.reservationId],
      workshopId: [null]

    });
    this.bookingForm.get('number_of_persons')?.valueChanges.subscribe(value => {
      this.getReservationData(value);
    });

    this.getReservationData(this.bookingForm.get('number_of_persons')?.value);
  }

  getReservationData(number_of_persons: number) {
    this.workshopService.getReservationDataById(this.reservationId).subscribe((data) => {
      this.reservation = data;
      this.workshopName = data.workshop.workshop_name;
      this.reservationImg = this.reservation.workshop.photos.length > 0 ? this.reservation.workshop.photos[0].url : null;
      const pricePerPlace = this.reservation.workshop.workshop_price;
      this.totalAmount = number_of_persons * pricePerPlace;

      this.bookingForm.patchValue({
        workshopId: this.reservation.workshop.ID_workshop,
        total_price: this.totalAmount
      });
    })
  }

  submitBooking() {
    if (this.bookingForm.valid) {
      this.isLoading = true;
      console.log(this.bookingForm.value);
      console.log(this.cookieService.get('authentication'))
      this.workshopService.bookingWorkshop(this.cookieService.get('authentication'), this.bookingForm.value).subscribe({
        next: () => {
          this.isLoading = false;
          this.router.navigate(['booking-success']);
        },
        error: (err) => {
          this.isLoading = false;
          console.error(err);
        }
      });
    }
  }

  goToStep(step: number) {
    if (step <= this.currentStep) {
      this.currentStep = step;
    }
  }

  nextStep() {
    if (this.currentStep < 3) {
      if (this.currentStep === 1 && this.validateStep(this.currentStep)) {
        this.stepsValidated[this.currentStep - 1] = true;
        this.authService.validateToken(this.cookieService?.get('authentication')).subscribe((response: any) => {
          if (response.valid) {
            this.currentStep++;
          } else {
            this.router.navigate(['connect-to-your-account']);
          }
        });
      } else if (this.currentStep === 2 && this.validateStep(this.currentStep)) {
        this.stepsValidated[this.currentStep - 1] = true;
        this.currentStep++;
      }
    }
  }

  previousStep() {
    if (this.currentStep > 1) {
      this.currentStep--;
    }
  }

  validateStep(step: number): boolean {
    switch (step) {
      case 1:
        return this.bookingForm.valid;
      case 2:
        return true;
      case 3:
        return true;
      default:
        return false;
    }
  }

  formatTime(time: string): Date {
    if (!time) {
      return new Date();
    }

    const [hours, minutes] = time.split(':').map(Number);
    const date = new Date();
    date.setHours(hours, minutes, 0);
    return date;
  }

  toggleBlockCb(event: Event) {
    event.stopPropagation();
    this.blockCbExpanded = !this.blockCbExpanded;
    if (this.blockCbExpanded) {
      this.blockPayPalExpanded = false;
    }
  }

  toggleBlockPayPal(event: Event) {
    event.stopPropagation();
    this.blockPayPalExpanded = !this.blockPayPalExpanded;
    if (this.blockPayPalExpanded) {
      this.blockCbExpanded = false;
    }
  }
}
