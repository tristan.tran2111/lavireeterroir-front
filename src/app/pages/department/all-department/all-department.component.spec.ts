import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllDepartmentComponent } from './all-department.component';
import { HttpClientModule } from '@angular/common/http';

describe('AllDepartmentComponent', () => {
  let component: AllDepartmentComponent;
  let fixture: ComponentFixture<AllDepartmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientModule]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AllDepartmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
