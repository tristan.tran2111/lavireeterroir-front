import { Component, OnInit } from '@angular/core';
import { DepartmentService } from '../../../../services/department/department.service';
import { Department } from '../../../../models/department.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all-department',
  standalone: true,
  imports: [],
  templateUrl: './all-department.component.html',
  styleUrl: './all-department.component.scss'
})
export class AllDepartmentComponent implements OnInit {

  departmentList!: Department[];

  constructor(
    private departmentService: DepartmentService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getDepartments();
  }

  getDepartments() {
    this.departmentService.getAllDepartments().subscribe((data) => {
      this.departmentList = data;
    })
  }

  navigateToDepartment(departmentCode: number) {
    this.router.navigate(['/department', departmentCode]);
  }
}
