import { Component, OnInit, inject } from '@angular/core';
import { DepartmentService } from '../../../services/department/department.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ProductService } from '../../../services/products/product.service';
import { Product, ProductType } from '../../../models/product.model';
import { ButtonComponent } from '../../component/button/button.component';
import { PaginationComponent } from '../../component/pagination/pagination.component';
import { Department } from '../../../models/department.model';
import { WorkshopService } from '../../../services/workshop/workshop.service';
import { BlogService } from '../../../services/blog/blog.service';
import { Blog } from '../../../models/blog.model';
import { CircuitService } from '../../../services/circuit/circuit.service';

@Component({
  selector: 'app-department',
  standalone: true,
  imports: [CommonModule, ButtonComponent, PaginationComponent],
  templateUrl: './department.component.html',
  styleUrl: './department.component.scss'
})
export class DepartmentComponent implements OnInit {
  department_data!: Department;
  department_code: any;
  type!: ProductType[];
  productsToShow: Product[] = [];
  product: Product[] = [];
  workshop: any[] = [];
  blog!: Blog[];
  circuit!: any[];
  currentPage: number = 1;
  itemsPerPage: number = 10;
  totalItems: number = 0;
  displayedWorkshopCount: number = 3;
  private route = inject(ActivatedRoute);

  constructor(
    private departmentService: DepartmentService,
    private productService: ProductService,
    private worskhopService: WorkshopService,
    private blogService: BlogService,
    private circuitService: CircuitService,
    private router: Router
  ){}

  ngOnInit() {
    this.department_code = this.route.snapshot.paramMap.get('department_code');
    this.fetchDepartmentData();
    this.fetchAllProductType();
    this.fetchWorkshopByDepartmentCode(this.department_code);
    this.fetchProductByDepartmentCode(this.department_code);
    this.fetchBlogByDepartmentCode(this.department_code);
    this.fetchCircuitByDepartmentCode(this.department_code);
    window.scrollTo(0, 0);
  }

  fetchDepartmentData(){
    this.departmentService.getDepartment(this.department_code).subscribe((data) => {
      this.department_data = data;
    })
  }

  fetchAllProductType(){
    this.productService.getAllProductsType().subscribe((data) => {
      this.type = data;
    })
  }

  fetchProductByDepartmentCode(code: number){
    this.productService.getProductByDepartmentCode(code).subscribe((data) => {
      this.product = data;
      this.totalItems = data.length;
      this.updateProductsToShow();
    })
  }

  fetchWorkshopByDepartmentCode(code: number){
    this.worskhopService.getWorkshopByDepartmentCode(code).subscribe((data) => {
      this.workshop = data;
    })
  }

  fetchBlogByDepartmentCode(code:number){
    this.blogService.getBlogsByDepartmentCode(code).subscribe((data) => {
      this.blog = data;
    })
  }

  fetchCircuitByDepartmentCode(code: number){
    this.circuitService.getCircuitByDepartmentCode(code).subscribe((data) => {
      this.circuit = data;
    })
  }

  updateProductsToShow() {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    this.productsToShow = this.product.slice(startIndex, startIndex + this.itemsPerPage);
  }

  onPageChange(pageNumber: number) {
    this.currentPage = pageNumber;
    this.updateProductsToShow();
  }

  navigateToProduct(productName: string) {
    this.router.navigate(['/product', productName]);
    window.scrollTo(0, 0);
  }

  navigateToBlog(articleTitle: string) {
    this.router.navigate(['/blog', articleTitle]);
    window.scrollTo(0, 0);
  }

  navigateToWorkshop(id: number) {
    this.router.navigate(['/workshop', id]);
    window.scrollTo(0, 0);
  }

  navigateToCircuit(id: number) {
    this.router.navigate(['/circuit', id]);
    window.scroll(0, 0);
  }

  truncateText(text: string, length: number): string {
    return text.length > length ? text.slice(0, length) + '...' : text;
  }

  showMoreWorkshop() {
    this.displayedWorkshopCount = this.workshop.length;
  }
}
