import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './contact.component.html',
  styleUrl: './contact.component.scss'
})
export class ContactComponent {
  contactForm!: FormGroup;

  constructor(private formBuilder: FormBuilder){
    this.contactForm = this.formBuilder.group({
      user_mail: new FormControl('', {validators: [Validators.required]}),
      tel: new FormControl('', {validators: [Validators.required]}),
      firstname: new FormControl('', {validators: [Validators.required]}),
      lastname: new FormControl('', {validators: [Validators.required]}),
      object: new FormControl('', {validators: [Validators.required]}),
      content: new FormControl('Bonjour,', {validators: [Validators.required]})
    });
  }

  onSubmit(){

  }
}
