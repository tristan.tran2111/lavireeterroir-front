import { Component, OnInit, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BlogService } from '../../../../services/blog/blog.service';
import { Blog } from '../../../../models/blog.model';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-display-blog',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './display-blog.component.html',
  styleUrl: './display-blog.component.scss'
})
export class DisplayBlogComponent implements OnInit {
  name: any;
  blog!: Blog;

  private route = inject(ActivatedRoute);

  constructor(
    private blogService: BlogService
  ) {
    this.name = this.route.snapshot.paramMap.get('name');
   }

  ngOnInit() {
    this.blogService.getBlogByName(this.name).subscribe((data) => {
      this.blog = data;
    })
  }


}
