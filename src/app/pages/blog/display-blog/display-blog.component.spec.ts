import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayBlogComponent } from './display-blog.component';
import { ActivatedRoute } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

describe('DisplayBlogComponent', () => {
  let component: DisplayBlogComponent;
  let fixture: ComponentFixture<DisplayBlogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            // Mock ActivatedRoute
            snapshot: {
              paramMap: {
                get: () => '1' // Return whatever value you want for paramMap.get('id')
              }
            }
          }
        }
      ]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DisplayBlogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
