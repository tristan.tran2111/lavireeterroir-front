import { Component } from '@angular/core';
import { BlogService } from '../../../services/blog/blog.service';
import { PaginationComponent } from '../../component/pagination/pagination.component';
import { Blog } from '../../../models/blog.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-blog',
  standalone: true,
  imports: [PaginationComponent],
  templateUrl: './blog.component.html',
  styleUrl: './blog.component.scss'
})
export class BlogComponent {
  article!: Blog[];
  currentPage: number = 1;
  itemsPerPage: number = 3;
  totalItems: number = 0;

  constructor(
    private blogService: BlogService,
    private router: Router
  ){
    this.fetcharticle();
    this.fetchAllArticle();
  }

  fetchAllArticle(){
    this.blogService.getAllBlogs().subscribe((article) => {
      this.totalItems = article.length;
    });
  }

  fetcharticle() {
    this.blogService.getPaginatedBlogs(this.currentPage, this.itemsPerPage)
      .subscribe((response) => {
        this.article = response;
      });
  }

  onPageChange(page: number) {
    this.currentPage = page;
    this.fetcharticle();
  }

  navigateToBlog(articleTitle: string) {
    this.router.navigate(['/blog', articleTitle]);
    window.scrollTo(0, 0);
  }
}
