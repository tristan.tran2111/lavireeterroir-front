import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router'; // Import ActivatedRoute
import { CreatedSuccessComponent } from './created-success.component';
import { HttpClientModule } from '@angular/common/http';

describe('CreatedSuccessComponent', () => {
  let component: CreatedSuccessComponent;
  let fixture: ComponentFixture<CreatedSuccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [], // Include CreatedSuccessComponent in declarations
      imports: [HttpClientModule],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            // Mock ActivatedRoute
            snapshot: {
              paramMap: {
                get: () => '1' // Return whatever value you want for paramMap.get('id')
              }
            }
          }
        }
      ]
    }).compileComponents();
    
    fixture = TestBed.createComponent(CreatedSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
