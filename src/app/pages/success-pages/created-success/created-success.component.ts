import { Component, OnInit, inject } from '@angular/core';
import { ButtonComponent } from '../../../component/button/button.component';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-created-success',
  standalone: true,
  imports: [ButtonComponent],
  templateUrl: './created-success.component.html',
  styleUrl: './created-success.component.scss'
})
export class CreatedSuccessComponent implements OnInit {

  token!: any;
  isGoodToken: boolean = false;
  badToken: boolean = false;
  private route = inject(ActivatedRoute);

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ){}
  
  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get('token');
    this.authenticationService.confirmUser(this.token).subscribe({
      next: () => {
        this.isGoodToken = true;
      },
      error: () => {
        this.badToken = true;
      }
    })
  }

  navigateToHomepage() {
    this.router.navigate(['/']);
  }

}
