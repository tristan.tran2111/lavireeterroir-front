import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommerceCreatedSuccessComponent } from './commerce-created-success.component';
import { HttpClientModule } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

describe('CommerceCreatedSuccessComponent', () => {
  let component: CommerceCreatedSuccessComponent;
  let fixture: ComponentFixture<CommerceCreatedSuccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CommerceCreatedSuccessComponent, HttpClientModule],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            // Mock ActivatedRoute
            snapshot: {
              paramMap: {
                get: () => '1' // Return whatever value you want for paramMap.get('id')
              }
            }
          }
        }
      ]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CommerceCreatedSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
