import { Component, OnInit, inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import { ButtonComponent } from '../../../component/button/button.component';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-commerce-created-success',
  standalone: true,
  imports: [ButtonComponent],
  templateUrl: './commerce-created-success.component.html',
  styleUrl: './commerce-created-success.component.scss'
})
export class CommerceCreatedSuccessComponent implements OnInit {
  token!: any;
  username!: string;
  isGoodToken: boolean = false;
  badToken: boolean = false;
  private route = inject(ActivatedRoute);

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private cookieService: CookieService
  ){}

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get('token');
    this.username = this.cookieService.get('username');
    this.authenticationService.confirmCreateMerchant(this.username, this.token).subscribe({
      next: () => {
        this.isGoodToken = true;
      },
      error: () => {
        this.badToken = true;
      }
    })
  }

  navigateToCommercePage() {
    this.router.navigate(['my-commerce']);
  }

}
