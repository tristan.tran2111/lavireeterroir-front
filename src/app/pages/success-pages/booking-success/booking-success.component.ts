import { Component } from '@angular/core';
import { ButtonComponent } from '../../../component/button/button.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-booking-success',
  standalone: true,
  imports: [ButtonComponent],
  templateUrl: './booking-success.component.html',
  styleUrl: './booking-success.component.scss'
})
export class BookingSuccessComponent {

  constructor(private router: Router){}
  
  navigateToProfile() {
    this.router.navigate(['/my-profile']);
    window.scrollTo(0, 0);
  }
}
