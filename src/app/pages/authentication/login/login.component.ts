import { Component } from '@angular/core';
import { ReactiveFormsModule, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import { CommonModule } from '@angular/common';
import { ModalComponent } from '../../../component/modal/modal.component';
import { ButtonComponent } from '../../../component/button/button.component';
import { SpinnerComponent } from '../../../component/spinner/spinner.component';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [ReactiveFormsModule, RouterLink, CommonModule, ModalComponent, ButtonComponent, SpinnerComponent],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  modalIsOpen: boolean = false;
  loginForm: FormGroup;
  forgotForm: FormGroup;
  isRequiredEmail: boolean = false;
  isEmailValid: boolean = false
  isRequiredForgotEmail: boolean = false;
  isEmailForgotValid: boolean = false
  isRequiredPassword: boolean = false;
  isinvalid: boolean = false;
  isinvalidForgotModal: boolean = false;
  isLoading: boolean = false;
  connexionProblem: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private router: Router
  ) {
    this.loginForm = this.formBuilder.group({
      user_mail: [localStorage.getItem('user_mail') || '', { validators: [Validators.required, Validators.email] }],
      password: [localStorage.getItem('password') || '', { validators: [Validators.required] }],
      rememberMe: [false]
    });

    this.forgotForm = this.formBuilder.group({
      user_mail: ['', { validators: [Validators.required, Validators.email] }]
    })
  }

  onSubmit() {
    if (this.loginForm.valid) {
      if (this.loginForm.value.rememberMe) {
        localStorage.setItem('user_mail', this.loginForm.value.user_mail);
        localStorage.setItem('password', this.loginForm.value.password);
      } else {
        localStorage.removeItem('user_mail');
        localStorage.removeItem('password');
      }
      this.isLoading = true;
      this.authenticationService.signIn(this.loginForm.value).subscribe({
        next: () => {
          this.isLoading = false;
          this.router.navigate(['my-profile'])
        },
        error: (err) => {
          this.isLoading = false;
          switch (err.error.message) {
            case 'Error connecting user': {
              this.isinvalid = true;
              break;
            }
            case 'Failed to fetch': {
              this.connexionProblem = true;
              break;
            }
            default: {
              break;
            }
          }
        }
      });
    } else {
      if (this.loginForm.get('user_mail')?.errors?.['required']) {
        this.isRequiredEmail = true;
      } else {
        this.isRequiredEmail = false;
      }

      if (this.loginForm.get('user_mail')?.errors?.['email']) {
        this.isEmailValid = true;
      } else {
        this.isEmailValid = false;
      }

      if (this.loginForm.get('password')?.errors?.['required']) {
        this.isRequiredPassword = true;
      } else {
        this.isRequiredPassword = false
      }
    }
  }

  onForgotSubmit(){
    if(this.forgotForm.valid) {
      this.isLoading = true;
      this.authenticationService.sendForgotEmail(this.forgotForm.value).subscribe({
        next: () => {
          this.isLoading = false;
          this.modalIsOpen = false;
        },
        error: () => {
          this.isLoading = false;
          this.isinvalidForgotModal = true;
        }
      })
    } else {
      if (this.forgotForm.get('mail_forgot')?.errors?.['required']) {
        this.isRequiredForgotEmail = true;
      } else {
        this.isRequiredForgotEmail = false;
      }

      if (this.forgotForm.get('mail_forgot')?.errors?.['email']) {
        this.isEmailForgotValid = true;
      } else {
        this.isEmailForgotValid = false;
      }
    }
  }

  openModal() {
    this.modalIsOpen = true;
  }
}
