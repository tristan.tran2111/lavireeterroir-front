import { Component, OnInit, inject } from '@angular/core';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ButtonComponent } from '../../../component/button/button.component';
import { map } from 'rxjs/operators';
import { SpinnerComponent } from '../../../component/spinner/spinner.component';

@Component({
  selector: 'app-forgot',
  standalone: true,
  imports: [ReactiveFormsModule, ButtonComponent, SpinnerComponent],
  templateUrl: './forgot.component.html',
  styleUrl: './forgot.component.scss'
})
export class ForgotComponent implements OnInit {
  token!: any;
  updateForm: FormGroup;
  valid: boolean = false;
  isRequiredPassword: boolean = false;
  isRequiredConfirm: boolean = false;
  isNotSame: boolean = false;
  isPasswordValid: boolean = false;
  isLoading: boolean = false;
  private route = inject(ActivatedRoute);

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private formBuilder: FormBuilder,
  ) {
    this.updateForm = this.formBuilder.group({
      password: ['', { validators: [Validators.required, Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*]).{8,}$")] }],
      confirmPassword: ['', { validators: [Validators.required]}],
    });
  }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get('token');
    this.authenticationService.validateToken(this.token).pipe(
      map(response => response.valid)
    ).subscribe(valid => {
      this.valid = valid;
    });
  }

  onUpdateSubmit() {
    if (this.updateForm.valid) {
      this.isLoading = true;
      this.authenticationService.resetPassword(this.token, this.updateForm.value).subscribe({
        next: () => {
          this.isLoading = false;
          this.router.navigate(['/']);
        },
        error: (err) => {
          this.isLoading = false;
          console.log(err)
        }
      })
    } else {
      const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*]).{8,}$/;

      if (this.updateForm.get('password')?.errors?.['required']) {
        this.isRequiredPassword = true;
      } else {
        this.isRequiredPassword = false;
      }

      if (!regex.test(this.updateForm.get('password')?.value)) {
        this.isPasswordValid = true;
      } else {
        this.isPasswordValid = false;
      }

      if (this.updateForm.get('password')?.value !== this.updateForm.get('confirmPassword')?.value) {
        this.isNotSame = true;
      } else {
        this.isNotSame = false;
      }

      if (this.updateForm.get('confirmPassword')?.errors?.['required']) {
        this.isRequiredConfirm = true;
      } else {
        this.isRequiredConfirm = false;
      }
    }
  }

  navigateToHomepage() {
    this.router.navigate(['/']);
  }
}
