import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCommerceComponent } from './create-commerce.component';
import { HttpClientModule } from '@angular/common/http';

describe('CreateCommerceComponent', () => {
  let component: CreateCommerceComponent;
  let fixture: ComponentFixture<CreateCommerceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CreateCommerceComponent, HttpClientModule]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CreateCommerceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
