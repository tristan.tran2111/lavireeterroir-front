import { Component } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import { Router } from '@angular/router';
import { SpinnerComponent } from '../../../component/spinner/spinner.component';
import { ModalComponent } from '../../../component/modal/modal.component';

@Component({
  selector: 'app-create-commerce',
  standalone: true,
  imports: [ReactiveFormsModule, ModalComponent, SpinnerComponent],
  templateUrl: './create-commerce.component.html',
  styleUrl: './create-commerce.component.scss'
})
export class CreateCommerceComponent {
  createCommerceForm: FormGroup;
  isRequiredName: boolean = false;
  isRequiredEmail: boolean = false;
  isRequiredTel: boolean = false;
  isRequiredAddress: boolean = false;
  isEmailValid: boolean = false;
  isLoading: boolean = false;
  modalInfo: boolean = false;
  isinvalid: boolean = false;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private formBuilder: FormBuilder,
  ) {
    this.createCommerceForm = this.formBuilder.group({
      merchant_name: ['', { validators: [Validators.required]}],
      merchant_email: ['', { validators: [Validators.required, Validators.email]}],
      merchant_phone: ['', { validators: [Validators.required]}],
      merchant_address: ['', { validators: [Validators.required]}]
    });
  }

  onCreateCommerceSubmit() {
    if (this.createCommerceForm.valid) {
      this.isLoading = true;
      this.authenticationService.sendMailcreateMerchant(this.createCommerceForm.value).subscribe({
        next: () => {
          this.isLoading = false;
          this.modalInfo = true;
        },
        error: (err) => {
          this.isLoading = false;
          this.isinvalid = true;
          console.log(err)
        }
      })
    } else {
      if (this.createCommerceForm.get('merchant_name')?.errors?.['required']) {
        this.isRequiredName = true;
      } else {
        this.isRequiredName = false;
      }

      if (this.createCommerceForm.get('merchant_mail')?.errors?.['required']) {
        this.isRequiredEmail = true;
      } else {
        this.isRequiredEmail = false;
      }

      if (this.createCommerceForm.get('merchant_mail')?.errors?.['email']) {
        this.isEmailValid = true;
      } else {
        this.isEmailValid = false;
      }

      if (this.createCommerceForm.get('tel')?.errors?.['required']) {
        this.isRequiredTel = true;
      } else {
        this.isRequiredTel = false;
      }

      if (this.createCommerceForm.get('address')?.errors?.['required']) {
        this.isRequiredAddress = true;
      } else {
        this.isRequiredAddress = false;
      }
    }
  }
}
