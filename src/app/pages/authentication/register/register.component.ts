import { Component } from '@angular/core';
import { ReactiveFormsModule, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from '../../../../services/authentication/authentication.service';
import { Router } from '@angular/router';
import { SpinnerComponent } from '../../../component/spinner/spinner.component';
import { ModalComponent } from '../../../component/modal/modal.component';

@Component({
  selector: 'app-register',
  standalone: true,
  imports: [ReactiveFormsModule, SpinnerComponent, ModalComponent],
  templateUrl: './register.component.html',
  styleUrl: './register.component.scss'
})
export class RegisterComponent {
  registerForm!: FormGroup;
  isinvalidName: boolean = false;
  isInvalidEmail: boolean = false;
  connexionProblem: boolean = false;
  isRequiredUsername: boolean = false;
  isRequiredEmail: boolean = false;
  isEmailValid: boolean = false
  isRequiredPassword: boolean = false;
  isPasswordValid: boolean = false;
  isRequiredConfirm: boolean = false;
  isNotSame: boolean = false;
  isNotChecked: boolean = false;
  isLoading: boolean = false;
  modalInfo: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private router: Router
  ) {
    this.registerForm = this.formBuilder.group({
      user_name: new FormControl('', { validators: [Validators.required] }),
      user_mail: new FormControl('', { validators: [Validators.required, Validators.email] }),
      password: new FormControl('', { validators: [Validators.required, Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*]).{8,}$")] }),
      confirmPassword: new FormControl('', { validators: [Validators.required] }),
      conditions: new FormControl(false, Validators.requiredTrue)
    });
  }

  onSubmit() {
    if (this.registerForm.valid) {
      this.isLoading = true;
      this.authenticationService.signUp(this.registerForm.value).subscribe({
        next: () => {
          this.isLoading = false;
          this.modalInfo = true;
        },
        error: (err) => {
          this.isLoading = false;
          switch (err.error.message) {
            case 'Username already exists': {
              this.isInvalidEmail = false;
              this.isinvalidName = true;
              break;
            }
            case 'Email already used': {
              this.isinvalidName = false;
              this.isInvalidEmail = true;
              break;
            }
            case 'Failed to fetch': {
              this.connexionProblem = true;
              break;
            }
            default: {
              break;
            }
          }
        }
      });
    } else {
      const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*]).{8,}$/;

      if (this.registerForm.get('user_name')?.errors?.['required']) {
        this.isRequiredUsername = true;
      } else {
        this.isRequiredUsername = false;
      }

      if (this.registerForm.get('user_mail')?.errors?.['required']) {
        this.isRequiredEmail = true;
      } else {
        this.isRequiredEmail = false;
      }

      if (this.registerForm.get('user_mail')?.errors?.['email']) {
        this.isEmailValid = true;
      } else {
        this.isEmailValid = false;
      }

      if (this.registerForm.get('password')?.errors?.['required']) {
        this.isRequiredPassword = true;
      } else {
        this.isRequiredPassword = false;
      }

      if (!regex.test(this.registerForm.get('password')?.value)) {
        this.isPasswordValid = true;
      } else {
        this.isPasswordValid = false;
      }

      if (this.registerForm.get('confirmPassword')?.errors?.['required']) {
        this.isRequiredConfirm = true;
      } else {
        this.isRequiredConfirm = false;
      }

      if (this.registerForm.get('password')?.value !== this.registerForm.get('confirmPassword')?.value) {
        this.isNotSame = true;
      } else {
        this.isNotSame = false;
      }

      if (!this.registerForm.get('conditions')?.value) {
        this.isNotChecked = true;
      } else {
        this.isNotChecked = false;
      }
    }
  }
}
