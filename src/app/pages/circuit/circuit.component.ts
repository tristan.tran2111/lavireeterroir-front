import { Component, OnInit, inject } from '@angular/core';
import { ButtonComponent } from '../../component/button/button.component';
import { CircuitService } from '../../../services/circuit/circuit.service';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { ActivatedRoute } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CookieService } from 'ngx-cookie-service';
import { SpinnerComponent } from '../../component/spinner/spinner.component';
import { PopupComponent } from '../../component/popup/popup.component';

@Component({
  selector: 'app-circuit',
  standalone: true,
  imports: [
    ButtonComponent, 
    CommonModule,
    SpinnerComponent,
    PopupComponent
  ],
  templateUrl: './circuit.component.html',
  styleUrls: ['./circuit.component.scss']
})
export class CircuitComponent implements OnInit {

  imgbackground!: string;
  idCircuit!: any;
  circuit: any;
  isCircuitFollowed: any;
  isLoading: boolean = false;
  successMessage: string = '';
  private route = inject(ActivatedRoute);

  constructor(
    private circuitService: CircuitService,
    private authenticationService: AuthenticationService,
    private cookieService: CookieService
  ) {
    this.idCircuit = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.getCircuitData();
    this.checkIfCircuitIsFollowed();
  }

  getCircuitData() {
    this.circuitService.getCircuitById(this.idCircuit).subscribe({
      next: (data) => {
        console.log(data)
        this.circuit = data;
      },
      error: () => {
        // Handle error
      }
    });
  }

  checkIfCircuitIsFollowed() {
    const token = this.cookieService.get('authentication');
    if (token) {
      this.authenticationService.isCircuitFollowedByUser(token, this.idCircuit).subscribe({
        next: (isFollowed) => {
          this.isCircuitFollowed = isFollowed.followed;
          console.log(this.isCircuitFollowed)
        },
        error: () => {
          // Handle error
        }
      });
    }
  }

  toggleCircuitFollow() {
    const token = this.cookieService.get('authentication');
    if (!token) return;

    if (this.isCircuitFollowed === true) {
      this.isLoading = true;
      this.authenticationService.removeCircuitFromUser(token, this.idCircuit).subscribe({
        next: () => {
          this.isCircuitFollowed = false;
          this.isLoading = false;
        },
        error: () => {
          // Handle error
        }
      });
    } else {
      this.isLoading = true;
      this.authenticationService.addCircuitToUser(token, this.idCircuit).subscribe({
        next: () => {
          this.isCircuitFollowed = true;
          this.isLoading = false;
          this.successMessage = 'Circuit suivi !';
          setTimeout(() => {
            this.successMessage = '';
          }, 5000);
        },
        error: () => {
          // Handle error
        }
      });
    }
  }
}
