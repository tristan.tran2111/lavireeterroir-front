import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';
import { WorkshopComponent } from './workshop.component';
import { WorkshopService } from '../../../services/workshop/workshop.service';
import { ActivatedRoute } from '@angular/router';
import { GeocodingService } from '../../../services/leaflet/geocoding.service';
import { ReviewService } from '../../../services/review/review.service';
import { CookieService } from 'ngx-cookie-service';
import { ReactiveFormsModule } from '@angular/forms';

describe('WorkshopComponent', () => {
  let workshopServiceMock: any;

  beforeEach(async () => {
    workshopServiceMock = {
      getWorkshopById: jasmine.createSpy('getWorkshopById').and.returnValue(of({
        workshop_address: '123 Main St',
        photos: [{ url: 'photo1.jpg' }, { url: 'photo2.jpg' }],
        reviews: [{ rating: 4 }, { rating: 5 }]
      }))
    };

    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        WorkshopComponent, // Import the standalone component
        ReactiveFormsModule
      ],
      providers: [
        { provide: WorkshopService, useValue: workshopServiceMock },
        GeocodingService,
        ReviewService,
        CookieService,
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { paramMap: { get: () => '1' } }
          }
        }
      ]
    }).compileComponents();
  });

  it('should pass', () => {
    expect(true).toBe(true);
  });
});
