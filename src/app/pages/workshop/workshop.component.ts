import { Component, OnInit } from '@angular/core';
import { WorkshopService } from '../../../services/workshop/workshop.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReservationComponent } from '../../component/reservation/reservation.component';
import { StarRatingComponent } from '../../component/star-rating/star-rating.component';
import { GalleryComponent } from '../../component/gallery/gallery.component';
import { GeocodingService } from '../../../services/leaflet/geocoding.service';
import { LeafletMapComponent } from '../../component/leaflet-map/leaflet-map.component';
import { ButtonComponent } from '../../component/button/button.component';
import { ReviewService } from '../../../services/review/review.service';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ModalComponent } from '../../component/modal/modal.component';
import { CookieService } from 'ngx-cookie-service';
import { SpinnerComponent } from '../../component/spinner/spinner.component';

@Component({
  selector: 'app-workshop',
  standalone: true,
  imports: [
    CommonModule, 
    ReservationComponent,
    StarRatingComponent,
    GalleryComponent,
    LeafletMapComponent,
    ButtonComponent,
    ModalComponent,
    ReactiveFormsModule,
    SpinnerComponent
  ],
  templateUrl: './workshop.component.html',
  styleUrls: ['./workshop.component.scss']
})
export class WorkshopComponent implements OnInit {
  workshop: any = {};
  workshopImageUrl!: string | null;
  isReservationOpen: boolean = false;
  workshopId!: number;
  icon: string = '<i class="fa-solid fa-plus text-lg"></i>'
  latitude!: number;
  longitude!: number;
  isReviewOpen: boolean = false;
  averageRating!: number;
  isLoading: boolean = false;
  isGalleryOpen: boolean = false;
  photoUrls: string[] = [];
  token!: string;
  reviewForm!: FormGroup;
  displayedReviewsCount: number = 3;

  constructor(
    private workshopService: WorkshopService,
    private route: ActivatedRoute,
    private geocodingService: GeocodingService,
    private reviewService: ReviewService,
    private cookieService: CookieService,
    private router: Router,
    private fb: FormBuilder
  ){
    this.workshopId = +this.route.snapshot.paramMap.get('id')!;
    this.token = this.cookieService.get('authentication');
    this.reviewForm = this.fb.group({
      rating: [null, [Validators.required]],
      content: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    if (this.workshopId) {
      this.workshopService.getWorkshopById(this.workshopId).subscribe((data) => {
        this.workshop = data || {};
        this.geocodeAddress(this.workshop.workshop_address);
        this.workshopImageUrl = this.workshop.photos && this.workshop.photos.length > 0 ? this.workshop.photos[0].url : null;
        this.photoUrls = this.workshop.photos ? this.workshop.photos.map((photo: any) => photo.url) : [];
        this.workshop.reviews = this.workshop.reviews || [];
        this.calculateAverageRating();
      });
    }
  }

  calculateAverageRating(): void {
    if (this.workshop && Array.isArray(this.workshop.reviews) && this.workshop.reviews.length > 0) {
      const totalRating = this.workshop.reviews.reduce((sum: number, review: { rating: number }) => sum + review.rating, 0);
      this.averageRating = parseFloat((totalRating / this.workshop.reviews.length).toFixed(1));
    } else {
      this.averageRating = 0;
    }
  }

  geocodeAddress(address: string) {
    this.geocodingService.geocode(address).subscribe((data: any) => {
      if (data && data.length > 0) {
        this.latitude = parseFloat(data[0].lat);
        this.longitude = parseFloat(data[0].lon);
        console.log(`Latitude: ${this.latitude}, Longitude: ${this.longitude}`);
      } else {
        console.log('No coordinates found for the given address');
      }
    },
      (error) => {
        console.error('Error occurred while geocoding the address:', error);
      });
  }

  showMoreReviews() {
    if (Array.isArray(this.workshop.reviews)) {
      this.displayedReviewsCount = this.workshop.reviews.length;
    }
  }

  openReservation() {
    this.isReservationOpen = true;
  }

  closeReservation() {
    this.isReservationOpen = false;
  }

  openGallery() {
    this.isGalleryOpen = true;
  }

  openReview() {
    this.isReviewOpen = true;
  }

  closeReview() {
    this.isReviewOpen = false;
  }

  submitReview() {
    if (!this.token) {
      this.router.navigate(['/connect-to-your-account']);
      window.scrollTo(0, 0);
    } else {
      const formValue = this.reviewForm.value;
      this.isLoading = true;
      this.reviewService.addAWorshopComment(this.token, this.workshopId, formValue).subscribe({
        next: () => {
          this.isLoading = false;
          window.location.reload();
        },
        error: (err) => {
          this.isLoading = false;
          console.error(err);
        }
      });
    }
  }
}
