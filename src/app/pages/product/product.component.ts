import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../services/products/product.service';
import { Product } from '../../../models/product.model';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { Location } from '@angular/common';
import { LeafletMapComponent } from '../../component/leaflet-map/leaflet-map.component';

@Component({
  selector: 'app-product',
  standalone: true,
  imports: [CommonModule, LeafletMapComponent],
  templateUrl: './product.component.html',
  styleUrl: './product.component.scss'
})
export class ProductComponent implements OnInit {
  product!: Product;
  productName!: string | null;

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router
  ) {
    this.productName = this.route.snapshot.paramMap.get('product_name');
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    if (this.productName !== null) {
      this.productService.getProductByName(this.productName).subscribe((data) => {
        this.product = data;
      });
    }
  }

  goBack(): void {
    this.location.back();
    window.scrollTo(0, 0);
  }

  navigateToMerchant(merchantName: string) {
    this.router.navigate(['/merchant', merchantName]);
    window.scrollTo(0, 0);
  }
}
