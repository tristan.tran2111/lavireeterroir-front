import { Component } from '@angular/core';
import { LeafletMapComponent } from '../../../component/leaflet-map/leaflet-map.component';

@Component({
  selector: 'app-about',
  standalone: true,
  imports: [LeafletMapComponent],
  templateUrl: './about.component.html',
  styleUrl: './about.component.scss'
})
export class AboutComponent {

}
