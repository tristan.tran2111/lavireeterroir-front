import { Routes } from '@angular/router';
import { HomeComponent } from './layout/home/home.component';
import { BlogComponent } from './pages/blog/blog.component';
import { LoginComponent } from './pages/authentication/login/login.component';
import { RegisterComponent } from './pages/authentication/register/register.component';
import { ContactComponent } from './pages/contact/contact.component';
import { UserComponent } from './pages/profile/user/user.component';
import { authGuard } from '../guard/auth-guard.guard';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { DepartmentComponent } from './pages/department/department.component';
import { ProductComponent } from './pages/product/product.component';
import { CreatedSuccessComponent } from './pages/success-pages/created-success/created-success.component';
import { CGUComponent } from './pages/static/cgu/cgu.component';
import { ForgotComponent } from './pages/authentication/forgot/forgot.component';
import { CommerceComponent } from './pages/profile/commerce/commerce.component';
import { commerceGuard } from '../guard/commerce.guard';
import { CreateCommerceComponent } from './pages/authentication/create-commerce/create-commerce.component';
import { CommerceCreatedSuccessComponent } from './pages/success-pages/commerce-created-success/commerce-created-success.component';
import { MerchantComponent } from './pages/merchant/merchant.component';
import { AllDepartmentComponent } from './pages/department/all-department/all-department.component';
import { AboutComponent } from './pages/static/about/about.component';
import { WorkshopComponent } from './pages/workshop/workshop.component';
import { BookingComponent } from './pages/booking/booking.component';
import { BookingSuccessComponent } from './pages/success-pages/booking-success/booking-success.component';
import { AdminComponent } from './pages/admin/admin.component';
import { adminGuard } from '../guard/admin.guard';
import { DisplayBlogComponent } from './pages/blog/display-blog/display-blog.component';
import { CircuitComponent } from './pages/circuit/circuit.component';

export const routes: Routes = [
  {'path': 'department/:department_code', component: DepartmentComponent},
  {'path': 'product/:product_name', component: ProductComponent},
  {'path': 'merchant/:merchant_name', component: MerchantComponent},
  {'path': '', component: HomeComponent},
  {'path': 'create-an-account', component: RegisterComponent},
  {'path': 'workshop/:id', component: WorkshopComponent},
  {'path': 'booking/:id', component: BookingComponent},
  {'path': 'all-department', component: AllDepartmentComponent},
  {'path': 'connect-to-your-account', component: LoginComponent},
  {'path': 'notre-blog', component: BlogComponent},
  {'path': 'blog/:name', component: DisplayBlogComponent },
  {'path': 'conditions-générales-dutilisation', component: CGUComponent},
  {'path': 'contact', component: ContactComponent},
  {'path': 'about-us', component: AboutComponent},
  {'path': 'created-successfully/:token', component: CreatedSuccessComponent},
  {'path': 'commerce-created-successfully/:token', component: CommerceCreatedSuccessComponent},
  {'path': 'booking-success', component: BookingSuccessComponent},
  {'path': 'forgot-password/:token', component: ForgotComponent},
  {'path': 'my-profile', component: UserComponent, canActivate: [authGuard]},
  {'path': 'create-a-commerce', component: CreateCommerceComponent},
  {'path': 'my-commerce', component: CommerceComponent, canActivate: [commerceGuard]},
  {'path': 'admin', component: AdminComponent, canActivate: [adminGuard]},
  {'path': 'circuit/:id', component: CircuitComponent},
  { path: '**', component: NotFoundComponent }
];
